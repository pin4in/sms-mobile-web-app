# AngularJS SMS WebApp #

Web application for managing sms chat.


### Technologies ###

* AngularJS
    * UI-router
    * Infinit Scroll
    * Toaster
    * ngAnimate
    * Sharing scope data using service

* Sass
* Bower
* Bootstrap