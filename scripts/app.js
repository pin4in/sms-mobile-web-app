var app = angular.module('sms-app' , [ 
	'ui.router',
	'infinite-scroll',
	'toaster',
	'ngAnimate'
]);


app.config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('inbox', {
			url: "/",
			templateUrl: 'templates/inbox.html',
			controller: 'ListDialogs'
		})
		.state('dialogs', {
			url: "/dialogs/:sender",
			templateUrl: 'templates/dialogs.html',
			controller: 'DialogsController'
		})
		.state('chat', {
			url: "/chat/:sender?template",
			templateUrl: 'templates/chat.html',
			controller: 'MessengerController'
		})
		.state('contacts', {
			url: "/contacts",
			templateUrl: 'templates/contacts.html',
			controller: 'ContactsController'
		})
		.state('contact', {
			url: "/contact/:sender",
			templateUrl: 'templates/contact.html',
			controller: 'ContactController'
		})
		.state('new-contact', {
			url: "/new-contact/:sender",
			templateUrl: 'templates/new-contact.html',
			controller: 'NewContactController'
		})
		.state('templates', {
			url: "/templates/:sender",
			templateUrl: 'templates/templates.html',
			controller: 'TemplatesController'
		})
		.state('templateslist', {
			url: "/templates-list/:sender",
			templateUrl: 'templates/templateslist.html',
			controller: 'TemplatesController'
		})
		.state('template', {
			url: "/template/:template",
			templateUrl: 'templates/template.html',
			controller: 'TemplateController'
		})
		.state('new-template', {
			url: "/new-template/",
			templateUrl: 'templates/new-template.html',
			controller: 'NewTemplateController'
		})

	$urlRouterProvider.otherwise('/');
});

app.directive('loadChatEnd', function($timeout) {
		 return {
				restrict: 'A',
				link: function (scope, element, attr) {
					if (scope.$last === true) {
						scope.$evalAsync(attr.loadChatEnd);
					}
			}
		}
	}
);
// pagination for chat page - IN PROGRESS
app.directive("scroll", function () {
    return function(scope, element) {
        element.bind("scroll", function() {
             if (this.scrollTop <= 300) {
             	if(scope.contact.hasMore && !scope.contact.isLoading)
	                 scope.contact.loadMoreChat();
             }
        });
    };
});

app.service('ContactService', function($http, $interval, $rootScope, toaster) {
	var self = {
		'sender': null,
		'template': 'empty',
		'templateText': null,
		'textMessage': "",
		'search': null,
		'contacts': [],
		'messages': [],
		'page': 1,
		'hasMore': true,
		'isLoading': false,
		'interval': null,
		'loadInboxUrl': $('.dialogs').attr('data-url'),
		'checkForNewUrl': $('.dialogs').attr('data-new'),
		'loadChatMessages': "",
		'toasterMessage': function(type, title, text) {
			toaster.pop({
            	type: type, 
            	title: title, 
            	body: text
            });
		},

		// INBOX SECTION
		'loadInbox': function() {
			if(self.hasMore && !self.isLoading)
				self.isLoading = true;
				var params = {
					'page': self.page,
					'search': self.search
				}
				self.loadInboxUrl = $('.dialogs').attr('data-url');
				$http.get(self.loadInboxUrl, {params: params}).success(function(data) {
					if(data.length > 0)
						angular.forEach(data, function (message) {
							self.messages.push(message);
						});
					else
						contact.hasMore = false;
					self.isLoading = false
				});
		},
		'loadNewInbox': function(data) {
			for (var i = 0; i < data.length; i++) {		
				self.messages.unshift(data[i]);
				self.toasterMessage('info' , 'Новое сообщение', 'Номер ' + data[i].from);
			};
		},
		'loadMoreInbox': function() {
			if (self.hasMore && !self.isLoading) {
				console.log('load more!');
				console.log('page number ' + self.page)
				self.page += 1;
				self.loadInbox();
			}
		},
		'timerStart': function() {
			console.log('Timer started!')
			self.checkForNewUrl = $('.dialogs').attr('data-new');
			self.interval = $interval(function(){
					$http.get(self.checkForNewUrl).success(function(data) {
						if(data.length > 0) {
							self.loadNewInbox(data);
						}
					});
					console.log('new message')
			}, 2000);
		},
		'stopInterval': function() {
			if(angular.isDefined(self.interval)){
				$interval.cancel(self.interval);
				console.log('interval stopped')
			}		
			
		},
		'doSearchInbox': function() {
			console.log('search');
			self.messages = [];
			self.loadInbox(self.loadInboxUrl);
		},
		'watchFilters': function () {
			$rootScope.$watch(function () {
				if(self.search != null)
					return self.search;
			}, function (newVal) {
				if (angular.isDefined(newVal)) {
					self.doSearchInbox();
				}
			});
		},

		// CHAT SECTION
		'loadChatUrl': "",
		'templateChatUrl':"",
		'templateInChat':"",
		'loadNewMessagesChatUrl': "",
		'loadMessages': function() {
			if(self.hasMore && !self.isLoading) {
				self.isLoading = true;
				var params = {
					'page': self.page,
				}
				$http.get(self.loadChatUrl, params)
					.success(function(data) {
						if(data.length > 0)
							angular.forEach(data, function (message) {
								self.messages.unshift(message);
							});
						else
							contact.hasMore = false;
							self.isLoading = false
					});
				if(self.templateInChat != "empty" ) {
					$http.get(self.templateChatUrl).success(function(data) {
						for (var i = 0; i < data.length; i++) {
							self.textMessage = data[i].about;
						}
					});
				}
			}
		},
		'loadNewMessages': function(data) {
			for (var i = 0; i < data.length; i++) {
				self.messages.push(data[i]);
				self.toasterMessage('info' , 'Новое сообщение', 'Номер ' + data[i].from);
			};
		},
		
		'chatTimerStart': function() {
			console.log('Timer started!')
			// var url = $('.dialogs').attr('data-new');
			self.interval = $interval(function(){
					$http.get(self.loadNewMessagesChatUrl).success(function(data) {
						if(data.length > 0) {
							self.loadNewMessages(data);
							console.log('new message')
						}
					});
			}, 2000);
		},
		'sendMessage': function( url ) {
			$http.post(url, self.textMessage)
				.success(function(data) {
					self.loadMessages( url );
					self.toasterMessage('info' , 'Сообщение отравлено', 'На номер ' + data[i].to);
				}).error(function(data) {
					self.toasterMessage('error' , 'Сообщение не отравлено!');
				});
		},
		'loadMoreChat': function() {
			if (self.hasMore && !self.isLoading) {
				console.log('load more!');
				console.log('page number ' + self.page)
				self.page += 1;
				self.loadMessages();
			}
		},
		
	};
	
	
	return self;
});




