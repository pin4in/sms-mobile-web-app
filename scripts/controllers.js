// LIST ALL DIALOGS

app.controller('ListDialogs' , function ( $scope , $http, ContactService, toaster ) {

	$scope.contact = ContactService;
	$scope.contact.loadInbox();
	$scope.contact.watchFilters();
	$scope.checkForNew = function() {
		$scope.contact.timerStart();
	};
	// $scope.checkForNew();
});


// LIST DIALOGS OF ONE CONTACT

app.controller('DialogsController' , function ($scope , $http, $stateParams, ContactService ) {
	$scope.contact = ContactService;
	$scope.contact.sender = $stateParams.sender;
	$scope.contact.stopInterval();

	$scope.loadDialogs = function(url) {
		$http.get(url).success(function(data) {
			$scope.dialogs = data;
		});
	};


});


// MESSAGES 

app.controller('MessengerController' , function ($scope , $http, $location, $state, $stateParams, ContactService) {
	$scope.contact = ContactService;
	$scope.contact.sender = $stateParams.sender;
	$scope.contact.template = $stateParams.template;
	$scope.init = function(messagesUrl, template, templateUrl, newMessagesUrl) {
		$scope.contact.loadChatUrl = messagesUrl;
		$scope.contact.templateChatUrl = template;
		$scope.contact.templateInChat = templateUrl;
		$scope.contact.loadNewMessagesChatUrl = newMessagesUrl;
		$scope.contact.hasMore = true;
		$scope.contact.page = 1;
		$scope.contact.loadMessages();
	}
	$scope.checkForNew = function() {
		$scope.contact.chatTimerStart();
	};
	// $scope.checkForNew();
	$scope.finished = function() {
		goBot();
		console.log('finished!');
	}


	// DELETE LAST MESSAGE
	$scope.deleteLastMessage = function(url) {
		console.log('delete');
		$http.post(url)
			.success(function() {
				$scope.contact.toasterMessage('success' , 'Сообщение удалено');
			}).error(function() {
				$scope.contact.toasterMessage('error' , 'При удалении сообщения возникла ошибка');
			});	
	}
	$scope.deleteDialog = function(url) {
		$http.post(url)
			.success(function() {
				$scope.contact.toasterMessage('success' , 'Диалог удален');
				$state.go('dialogs', {sender: $scope.contact.sender});
			}).error(function() {
				$scope.contact.toasterMessage('error' , 'При удалении диалога возникла ошибка');
				$state.go('dialogs', {sender: $scope.contact.sender});
			});
	}

});


// CONTACTS

app.controller('ContactsController' , function ($scope , $http, $rootScope, $stateParams, ContactService ) {
	
	$scope.contact = ContactService;
	$scope.contact.stopInterval();
	$scope.loadContacts = function(url) {
		var params = {
			'search': $scope.contact.search
		}
		$scope.url = url;
		$http.get(url, params).success(function(data) {
			$scope.contact.contacts = data;
		});
	}

	$scope.doSearch = function() {
		console.log('doSearch');
		$scope.contact.contacts = [];
		$scope.loadContacts($scope.url)
	}

	$scope.watchFilters = function () {
		$scope.$watch(function () {
			if($scope.contact.search != null)
				return $scope.contact.search;
		}, function (newVal) {
			if (angular.isDefined(newVal)) {
				$scope.doSearch();
			}
		});
	}
	$scope.watchFilters();
});

app.controller('ContactController' , function ($scope, $http, $stateParams, ContactService ) {
	$scope.contact = ContactService;
	$scope.contact.sender = $stateParams.sender;
	$scope.loadContact = function(url) {
		var params = {
					'phone': $scope.contact.phone
				}
		$http.get(url, params).success(function(data) {
			$scope.onecontact = data[0];
		});
	}
	$scope.contactUpdate = function(url) {
		$http.post(url, this)
			.success(function(data) {
				$scope.contact.toasterMessage('success' , 'Контакт обновлен');
			}).error(function(data) {
				$scope.contact.toasterMessage('error' , 'При обновлении контакта возникла ошибка');
			});
	}
	$scope.deleteContact = function(url) {
		$http.post(url, $scope.contact.sender)
			.success(function(data) {
				$scope.contact.toasterMessage('success' , 'Контакт удален');
				$state.go('inbox');
			}).error(function(data) {
				$scope.contact.toasterMessage('error' , 'При удалении контакта возникла ошибка');
			});
	}
});
app.controller('NewContactController' , function ($scope, $http, $stateParams, ContactService, toaster ) {
	$scope.contact = ContactService;
	$scope.contact.sender = $stateParams.sender;
	$scope.contact.stopInterval();
	$scope.name = "";
	$scope.phone = $scope.contact.sender;
	$scope.about = "";
	$scope.creatContact = function(url) {
			$http.post(url, [this.name, this.phone, this.about])
				.success(function(data) {
					$scope.contact.toasterMessage('success' , 'Контакт создан');
				}).error(function(data) {
					$scope.contact.toasterMessage('error' , 'При создании контакта возникла ошибка');
				});
		}
});

// Templates

app.controller('TemplatesController' , function ($scope , $http, $stateParams, ContactService ) {
	$scope.contact = ContactService;
	$scope.contact.sender = $stateParams.sender;
	$scope.contact.stopInterval();
	$scope.loadTemplates = function(url) {
		$http.get(url).success(function(data) {
			$scope.templates = data;
		});
	}	
});

app.controller('TemplateController' , function ($scope , $http, $stateParams, ContactService ) {	
	$scope.contact = ContactService;
	$scope.contact.template = $stateParams.template;
	$scope.loadTemplate = function(url, template) {
		var param = {
			'phone': $scope.contact.template
		}
		$http.get(url, param).success(function(data) {
			$scope.contact.templateText = data.about;
		});
	}
	$scope.loadTemplate();

	// UPDATE TEMPLATE
	$scope.updateTemplate = function(url) {
		$http.post(url, [this.contact.name, this.contact.templateText])
			.success(function() {
				$scope.contact.toasterMessage('success' , 'Шаблон шаблон обновлен');
			}).error(function() {
					$scope.contact.toasterMessage('error' , 'При обновлении шаблона возникла ошибка');
				});
	}

	// DELETE TEMPLATE
	$scope.deleteTemplate = function(url) {
		$http.post(url, [this.contact.name])
			.success(function() {
				$scope.contact.toasterMessage('success' , 'Шаблон удален');
			}).error(function() {
					$scope.contact.toasterMessage('error' , 'При удалении шаблона возникла ошибка');
				});
	}
});
app.controller('NewTemplateController' , function ($scope , $http, ContactService ) {	
	$scope.contact = ContactService;
	$scope.name = '';
	$scope.about = '';
	$scope.saveTemplate = function(url) {
		$http.post(url, [this.name, this.about])
			.success(function() {
				$scope.contact.toasterMessage('success' , 'Шаблон создан');
			}).error(function() {
					$scope.contact.toasterMessage('error' , 'При создании шаблона возникла ошибка');
				});
	}
});