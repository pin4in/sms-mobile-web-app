$(document).ready(function() {
	
	// show sidebar
	$('.show-menu').click(function(){
		var sidebar = $('.sidebar-wrapper');
		if(sidebar.hasClass('shown')){
			w = -sidebar.outerWidth();
			sidebar.css( 'right' , w ).removeClass('shown');
		}else{
			sidebar.css('right','0').addClass('shown');	
		}
	});


	// remove sidebar after click on link
	if($(window).width() < 993) {

		$('.page-container').on('click', '.sidebar__link', function() {
			var sidebar = $('.sidebar-wrapper');
			w = -sidebar.outerWidth();
			sidebar.css( 'right' , w ).removeClass('shown');
		});
	}


	// show chat menu
	$('.page-container').on('click', '.dh-menu', function(){
		var dialogMenu = $('.dialog-menu');
		dialogMenu.toggleClass('shown');
	});


	// show/remove searchbox
	$('.page-container').on('click', '.search-button', function() {
		$('.search-field').toggleClass( 'shown' );
	})

	// scoll bottom after load Chat page
	var goBot = function() {
		var chat = $(".chat");
		chat.scrollTop(chat.prop('scrollHeight'));
		var w_h = $(window).height();
		
		// header + chatHeader + sendMessageForm
		if ($(window).width() < 768)
			chat.height( w_h - 211);

	}
});

